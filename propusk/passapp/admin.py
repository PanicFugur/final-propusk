from django.contrib import admin
from .models import (UserRole, Role, Path, Waypoint,
                     PathToRole, RequestData, RequestStatus, RequestChangeLog)
# Register your models here.

admin.site.register(UserRole)
admin.site.register(Role)
admin.site.register(Path)
admin.site.register(Waypoint)
admin.site.register(PathToRole)
admin.site.register(RequestData)
admin.site.register(RequestChangeLog)
admin.site.register(RequestStatus)
