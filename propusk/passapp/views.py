from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import PasswordChangeForm, UserCreationForm
from django.contrib.auth.models import Group, User
from django.contrib.auth.views import update_session_auth_hash
from django.shortcuts import HttpResponse, get_object_or_404, redirect, render
from django.urls import reverse

from .cryo import notify, save_old_rd
from .decorators import user_is_creator
from .filters import RequestDataFilter
from .forms import (NewPathForm, NewReqForm, PathEditForm, RegistrationForm,
                    RequestEditForm, RoleForm, StartWaypointForm, UserRoleForm,
                    WaypointEditForm, SearchForm)
from .models import (Path, RequestChangeLog, RequestData, RequestStatus, Role,
                     UserRole, Waypoint)

# Create your views here.


# @login_required
def home(request):
    return render(request, 'passapp/home.html')


def signup3(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid() and usertoroleform.is_valid():
            new_user = form.save()
            new_users_role = UserRole()
            new_users_role.user = new_user
            guest_role = get_object_or_404(Role, role_name='guest')
            new_users_role.role = guest_role
            new_users_role.save()
            ggroup = Group.objects.get(name='User')
            ggroup.user_set.add(new_user)
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            messages.success(request, 'Пользователь зарегистрирован')
            return redirect('home')
        else:
            messages.error(request,
                           'Произошла ошибка, проверьте введенные данные')
            return redirect(reverse('signup'))
    else:
        form = RegistrationForm()
        usertoroleform = UserRoleForm()
    return render(request, 'passapp/signup.html', {
        'form': form,
         })


@user_passes_test(lambda u: u.groups.filter(name='Supervisor').count() != 0,
                  login_url='/wrongturnkiddo/')
@login_required
def rolecreate(request, role_id=None):
    if role_id:
        role = get_object_or_404(Role, pk=role_id)
        message = 'Роль %s изменена' % role.role_name
    else:
        role = None
        message = 'Роль создана'
    roleform = RoleForm(request.POST or None, instance=role)
    if request.method == 'POST' and roleform.is_valid():
            roleform.save()
            return render(request, 'passapp/message.html', {
                'MessageTitle': 'Успех',
                'Message': message,
                'returnlink': reverse('rolelist')
            })
    else:
        return render(request, 'passapp/rolemake.html', {'roleform': roleform})


def wrongturn(request):
    return render(request, 'passapp/message.html', {
                'MessageTitle': 'Ошибка',
                'Message': """Упс, вы похоже не туда свернули, 
                у вас недосточно прав для посещения данной страницы.""",
                'returnlink': '/'
            })


@login_required
@user_passes_test(lambda u: u.groups.filter(name='Supervisor').count() != 0,
                  login_url='/wrongturnkiddo/')
def manview(request):
    return render(request, 'passapp/management-home.html')


@login_required
@user_passes_test(lambda u: u.groups.filter(name='Supervisor').count() != 0,
                  login_url='/wrongturnkiddo/')
def rolelistview(request):
    rolelist = Role.objects.all()
    return render(request, 'passapp/management-rolelist.html',
                  {'role_list': rolelist})


@login_required
def accdetails(request):
    current_user = request.user
    try:
        role_connector = UserRole.objects.get(user=current_user)
        users_role = role_connector.role
    except UserRole.DoesNotExist:
        users_role = 'У пользователя нет роли, обратитесь к администратору.'
    return render(request, 'passapp/accountdetails.html', {
        'role': users_role
    })


@login_required
def changepassword(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Пароль обновлен!')
            return redirect(reverse('passchange'))
        else:
            messages.error(request, 'Пожалуйста, исправьте ошибки.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'passapp/changepassword.html', {
        'form': form
    })


@login_required
def createrequest(request, passtype):
    if request.method == 'POST':
        user_role = get_object_or_404(UserRole, user=request.user)
        curr_user_role = user_role.role
        print(curr_user_role)
        form = NewReqForm(
                          request.POST,
                          passtype=passtype,
                          userrole=curr_user_role)
        if form.is_valid():
            new_request_data = form.save(commit=False)
            new_request_data.creator = request.user
            new_request_data.path = form.cleaned_data['path']
            new_request_data.active = True
            new_request_data.save()
            path_start = get_object_or_404(Waypoint,
                                           path=new_request_data.path,
                                           position=0)
            stage_zero = RequestStatus(data=new_request_data,
                                       location=path_start, needsreview=False)
            stage_zero.save()
            next_stage = Waypoint.objects.\
                    filter(path=new_request_data.path,
                           position=stage_zero.location.position+1)
            if not next_stage:
                endpoint = get_object_or_404(Waypoint, position=777,
                                             path=new_request_data.path)
                finish = RequestStatus(data=new_request_data, location=endpoint,
                                       needsreview=True)
                finish.save()
                messages.success(request,
                                 """Заявка полностью
                                 прошла процесс одобрения""")
            else:
                loc = stage_zero.location.position+1
                next_waypoint = get_object_or_404(Waypoint, position=loc,
                                                  path=new_request_data.path)
                next_status = RequestStatus(data=new_request_data,
                                            location=next_waypoint,
                                            needsreview=True)
                next_status.save()
            messages.success(request, 'Запрос создан!')
            return redirect(reverse('req_details', kwargs={'req_id': new_request_data.id}))
        else:
            messages.error(request, 'Что-то пошло не так')
            return redirect(reverse('typepick'))
    else:
        user_role = get_object_or_404(UserRole, user=request.user)
        curr_user_role = user_role.role
        form = NewReqForm(passtype=passtype, userrole=curr_user_role)
        print(curr_user_role)

        return render(request, 'passapp/newrequestform.html', {
            'form': form,
            'returnlink': reverse('home')
        })


@login_required
def req_type_pick(request):
    return render(request, 'passapp/requesttypepick.html', {})


@login_required
def req_list(request):
    requestlist = RequestData.objects.filter(creator=request.user)
    return render(request, 'passapp/requestlist.html',
                  {'request_list': requestlist})


@login_required
@user_is_creator
def req_details(request, req_id):
    req = get_object_or_404(RequestData, pk=req_id)
    req_statuses = RequestStatus.objects.filter(data=req).\
        order_by('location__position')
    return render(request, 'passapp/req_details.html',
                  {
                    'req_data': req,
                    'req_statuses': req_statuses
                  })


@login_required
def discussion_queue(request):
    users_role_obj = get_object_or_404(UserRole, user=request.user)
    users_role = users_role_obj.role.role_name
    req_statuses = RequestStatus.objects\
        .filter(location__current_role__role_name=users_role, needsreview=True,
                data__active=True)
    return render(request, 'passapp/reqqueue.html',
                  {'req_statuses': req_statuses})


@login_required
def req_discussion(request, req_id):
    users_role_obj = get_object_or_404(UserRole, user=request.user)
    req = get_object_or_404(RequestData, pk=req_id)
    req_statuses = RequestStatus.objects.filter(data=req)
    if request.method == 'POST':
        req_last_status = get_object_or_404(RequestStatus,
                                            data=req,
                                            needsreview=True)
        if users_role_obj.role == req_last_status.location.current_role:
            if 'Approve' in request.POST:
                print('Request: approved')
                req_last_status.needsreview = False
                req_last_status.save()
                next_stage = Waypoint.objects.\
                    filter(path=req.path,
                           position=req_last_status.location.position+1)
                if not next_stage:
                    endpoint = get_object_or_404(Waypoint, position=777,
                                                 path=req.path)
                    finish = RequestStatus(data=req, location=endpoint,
                                           needsreview=True)
                    finish.save()
                    messages.success(request,
                                     """Заявка полностью
                                     прошла процесс одобрения""")
                    notify(req_last_status, req.creator)
                    return redirect(reverse('queue'))
                else:
                    loc = req_last_status.location.position+1
                    next_waypoint = get_object_or_404(Waypoint, position=loc,
                                                      path=req.path)
                    next_status = RequestStatus(data=req,
                                                location=next_waypoint,
                                                needsreview=True)
                    next_status.save()
                    messages.success(request, 'Вы одобрили заявку!')
                    notify(req_last_status, req.creator)
                    return redirect(reverse('queue'))
            elif 'Deny' in request.POST:
                req_last_status.needsreview = False
                req_last_status.save()
                notify(req_last_status, req.creator, approve=False)
                print('Request denied')
                endpoint = get_object_or_404(Waypoint, position=666,
                                             path=req.path)
                finish = RequestStatus(data=req, location=endpoint,
                                       needsreview=True)
                finish.save()
                messages.success(request,
                                 'Вы отклонили заявку')
                return redirect(reverse('queue'))
            elif 'Finish' in request.POST:
                req_last_status.needsreview = False
                req_last_status.save()
                req.active = False
                req.save()
                return redirect(reverse('queue'))
        else:
            messages.error(request,
                           'Данная заявка не требует одобрение вашей роли!')
            return redirect('queue')
    else:
        return render(request, 'passapp/req_disc.html',
                      {
                        'req_data': req,
                        'req_statuses': req_statuses,
                        'userrole': users_role_obj,
                      })


@login_required
def req_edit(request, req_id):
    req = get_object_or_404(RequestData, pk=req_id)
    form = RequestEditForm(instance=req)
    if request.method == 'POST':
        form = RequestEditForm(request.POST, instance=req)
        if form.is_valid():
            save_old_rd(req, request.user)
            form.save()
            messages.success(request, 'Изменения сохранены!')
            return redirect(reverse('req_details', kwargs={'req_id': req_id}))
        else:
            messages.error(request, 'Ошибка в введенных данных')
            return render(request, 'passapp/reqedit.html', {'form': form})
    else:
        return render(request, 'passapp/reqedit.html', {'form': form,
                                                        'req': req})


@login_required
def pathlist(request):
    paths = Path.objects.all()
    return render(request, 'passapp/pathlist.html',
                  {
                      'paths': paths,
                  })


@login_required
def pathdetails(request, path_id):
    path = get_object_or_404(Path, pk=path_id)
    waypoints = Waypoint.objects.filter(path=path).order_by('position')
    wpform = WaypointEditForm()
    if request.method == 'POST':
        if 'newwaypoint' in request.POST:
            wpform = WaypointEditForm(request.POST)
            if wpform.is_valid():
                newwaypoint = wpform.save(commit=False)
                newwaypoint.path = path
                newwaypoint.save()
        if 'deletewaypoint' in request.POST:
            waypoint = get_object_or_404(Waypoint,
                                         pk=request.POST['waypoint_id'])
            waypoint.delete()                             
    return render(request, 'passapp/pathdetails.html',
                  {
                    'path': path,
                    'waypoints': waypoints,
                    'wpform': wpform
                  })


@login_required
def pathediting(request, path_id=None):
    if path_id:
        path = get_object_or_404(Path, pk=path_id)
        pathform = PathEditForm(instance=path)
        pathinfo = path
        wpform = None
    else:
        path = None
        pathinfo = None
        pathform = NewPathForm()
        wpform = StartWaypointForm()
    if request.method == 'POST':
        if 'delete' in request.POST:
            rd = RequestData.objects.filter(path=path)
            if not rd:
                path.delete()
                messages.success(request, 'Маршрут удален')
                return redirect(reverse('pathlist'))
            else:
                messages.error(request, """Данный маршрут
                               используется заявками, удаление невозможно""")
                return render(request, 'passapp/pathedit.html',
                              {
                                'form': pathform,
                                'pathinfo': pathinfo,
                                'wpform': wpform
                              })
        else:
            if path_id:
                pathform = PathEditForm(request.POST)
            else:
                pathform = NewPathForm(request.POST)
                wpform = StartWaypointForm(request.POST)
            if pathform.is_valid():
                new_path = pathform.save()
                if wpform is not None:
                    first_waypoint = wpform.save(commit=False)
                    first_waypoint.position = 0
                    first_waypoint.path = new_path
                    first_waypoint.save()
                    bureau = get_object_or_404(Role, role_name='pass_bureau')
                    graveyard = get_object_or_404(Role, role_name='graveyard')
                    successwaypoint = Waypoint(position=777, path=new_path,
                                               current_role=bureau)
                    successwaypoint.save()
                    failpoint = Waypoint(position=666, path=new_path,
                                         current_role=graveyard)
                    failpoint.save()
                    messages.success(request, 'Успешно сохранено') 
                return redirect(reverse('pdetails',
                                        kwargs={'path_id': new_path.id}))
            else:
                messages.error('Ошибка при сохранении')
    else:
        return render(request, 'passapp/pathedit.html',
                      {
                          'form': pathform,
                          'pathinfo': pathinfo,
                          'wpform': wpform
                      })







# End of file
