from django.db import models
from django.contrib.auth.models import User, Group
from django.utils import timezone
from .misc import one_day_hence

# Create your models here.


class Role(models.Model):
    role_name = models.CharField(max_length=150, verbose_name='Роль')

    description = models.TextField(default='Описания не было предоставлено.',
                                   max_length=500)

    def __str__(self):
        return(self.role_name)


class UserRole(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                related_name='userrole')
    role = models.ForeignKey(Role, on_delete=models.CASCADE)

    def __str__(self):
        return(str(self.role))


class Path(models.Model):
    TYPES = (
        ('PE', 'Постоянный на человека'),
        ('PC', 'Постоянный на машину'),
        ('TE', 'Временный на человека'),
        ('TC', 'Временный на машину')
    )
    passtype = models.CharField(max_length=50, choices=TYPES,
                                default='US')
    name = models.CharField(max_length=220)
    description = models.TextField(null=True, blank=True)
    production = models.BooleanField(default=False)

    def __str__(self):
        return(self.name)


class Waypoint(models.Model):
    position = models.IntegerField()
    path = models.ForeignKey(Path, on_delete=models.CASCADE)
    current_role = models.ForeignKey(Role, on_delete=models.CASCADE)

    def __str__(self):
        return('%s : %s' % (self.position, self.current_role))


class PathToRole(models.Model):
    path = models.ForeignKey(Path, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)

    def __str__(self):
        return('%s к %s' % (self.path.name, self.role.role_name))


class RequestData(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    path = models.ForeignKey(Path, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=timezone.now)
    reg_number = models.CharField(max_length=20)
    recipients_name = models.CharField(max_length=150)
    start_date = models.DateTimeField(default=timezone.now)
    exp_date = models.DateTimeField(default=one_day_hence())
    active = models.BooleanField(default=False)

    def __str__(self):
        return('Запрос от %s %s от %s' % (
            self.creator.first_name, self.creator.last_name, self.timestamp)
            )


class RequestStatus(models.Model):
    data = models.ForeignKey(RequestData, on_delete=models.CASCADE)
    location = models.ForeignKey(Waypoint, on_delete=models.CASCADE)
    needsreview = models.BooleanField(default=False)

    def __str__(self):
        return(
            ('[%s] Запрос №%s, ответсвенный: %s' %
             (self.location.position, self.data.pk,
              self.location.current_role))
            )


class RequestChangeLog(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    path = models.ForeignKey(Path, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=timezone.now)
    reg_number = models.CharField(max_length=20)
    recipients_name = models.CharField(max_length=150)
    start_date = models.DateTimeField(default=timezone.now)
    exp_date = models.DateTimeField(default=one_day_hence())
    change_date = models.DateTimeField(default=timezone.now)
    change_author = models.CharField(max_length=150, default='None')

    
