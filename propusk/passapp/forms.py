from django.contrib.auth.models import User, Group
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone
from datetime import datetime
import pytz
from .models import (Role, RequestData,
                     RequestStatus, Path,
                     Waypoint, UserRole, PathToRole)
   

class RegistrationForm(UserCreationForm):
    email = forms.EmailField(label="Адрес электронной почты")
    first_name = forms.CharField(max_length=30, label="Имя", required=False)
    last_name = forms.CharField(max_length=30, label="Фамилия", required=False)

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username",
                  "password1", "password2", )


class UserRoleForm(forms.ModelForm):
    class Meta:
        model = UserRole
        fields = ('role',)
        labels = {
            "role": "Роль в системе"
        }


class RoleForm(forms.ModelForm):
    class Meta:
        model = Role
        fields = ('role_name',)


class NewReqForm(forms.ModelForm):
    path = forms.ModelChoiceField(queryset=None, empty_label=None, label="Маршрут (выберите первы предложенный в списке)")
    start_date = forms.DateTimeField(widget=forms.SelectDateWidget, label="Дата начала работы пропуска")
    exp_date = forms.DateTimeField(widget=forms.SelectDateWidget, label="Дата истечения работы")

    class Meta:
        model = RequestData
        fields = ('reg_number', 'recipients_name',
                  'start_date', 'exp_date', 'path')
        labels = {
            "reg_number": "Официальный номер (паспорта или автомобиля)",
            "recipients_name": "Имя получателя",
            "start_date": "Дата начала работы пропуска",
            "exp_date": "Дата истечения работы",
            "path": "Маршрут (выберите первы предложенный в списке)"
        }

    def __init__(self, *args, **kwargs):
        self.passtype = kwargs.pop('passtype')
        self.userrole = kwargs.pop('userrole')
        super(NewReqForm, self).__init__(*args, **kwargs)
        qs2 = Path.objects.filter(waypoint__current_role=self.userrole,
                                  waypoint__position=0, passtype=self.passtype)
        self.fields['path'].queryset = qs2
        

class RequestViewForm(forms.ModelForm):
    class Meta:
        model = RequestData
        fields = ('creator', 'path', 'timestamp', 'reg_number',
                  'recipients_name', 'start_date', 'exp_date', 'active')
        # Form for viewing the request data and only viewing


class RequestEditForm(forms.ModelForm):
    class Meta:
        model = RequestData
        fields = ('reg_number', 'recipients_name',
                  'start_date', 'exp_date')


class PathEditForm(forms.ModelForm):
    class Meta:
        model = Path
        fields = ('production',)


class NewPathForm(forms.ModelForm):
    class Meta:
        model = Path
        fields = ('name', 'description', 'passtype', 'production')
        labels = {
            "name": "Позиция",
            "description": "Роли",
            "passtype": "Тип пропуска",
            "production": "Рабочий?"
        }



class WaypointEditForm(forms.ModelForm):
    class Meta:
        model = Waypoint
        fields = ('position', 'current_role')
        labels = {
            "position": "Позиция",
            "current_role": "Роли"
        }


class StartWaypointForm(forms.ModelForm):
    class Meta:
        model = Waypoint
        fields = ('current_role', )
        labels = {
            "current_role": "Роль в системе"
        }


class SearchForm(forms.ModelForm):
    timestamp = forms.DateTimeField(widget=forms.SelectDateWidget)

    class Meta:
        model = RequestData
        fields = ('creator', 'timestamp', 'active', 'path')
