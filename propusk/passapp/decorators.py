from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User
from .models import RequestData


def user_is_creator(function):
    def wrap(request, *args, **kwargs):
        entry = RequestData.objects.get(pk=kwargs['req_id'])
        if entry.creator == request.user:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
