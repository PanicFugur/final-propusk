import django_filters
from .models import RequestData


class RequestDataFilter(django_filters.FilterSet):
    timestamp = django_filters.DateTimeFilter(name='date_created')
    
    class Meta:
        model = RequestData
        fields = ['creator', 'path', 'timestamp', 'active']