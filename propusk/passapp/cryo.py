from django.core.mail import EmailMultiAlternatives
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string

from .models import RequestChangeLog, RequestData, RequestStatus, Waypoint


def save_old_rd(request_data, user):
    cryo_data = RequestChangeLog(
        creator=request_data.creator,
        path=request_data.path,
        timestamp=request_data.timestamp,
        reg_number=request_data.reg_number,
        recipients_name=request_data.recipients_name,
        start_date=request_data.start_date,
        exp_date=request_data.exp_date,
        change_author=user
    )
    cryo_data.save()


def notify(old_status, user, approve=True):
    subj = 'Ваша заявка в подсистеме заказа пропусков'
    if approve:
        template = 'passapp/notificationemail.html'
    else:
        template = 'passapp/denialemail.html'
    from_email, to = 'noreply@passsubsustem.ru', user.email
    req_data = get_object_or_404(RequestData, pk=old_status.data.id)
    html_content = render_to_string(template,
                                    {
                                        'req_data': req_data,
                                        'old_status': old_status
                                    })
    msg = EmailMultiAlternatives(subj, html_content, from_email, [to])
    msg.send()
