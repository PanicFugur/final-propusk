"""propusk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from passapp import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls,),
    path('', views.home, name='home'),
    path('signin/', auth_views.login, {'template_name': 'passapp/signin.html'},
         name='signin'),
    path('signout/', auth_views.logout,
         {'next_page': '/'},
         name='signout'),
    path('signup/', views.signup3, name='signup'),
    path('wrongturnkiddo/', views.wrongturn, name='wrongturn'),
    path('management/', views.manview, name='management'),
    path('management/roles', views.rolelistview, name='rolelist'),
    path('management/roles/newrole', views.rolecreate, name='newrole'),
    path('management/roles/<int:role_id>', views.rolecreate, name='roleedit'),
    path('management/paths/', views.pathlist, name='pathlist'),
    path('management/paths/<int:path_id>', views.pathdetails, name='pdetails'),
    path('management/paths/newpath', views.pathediting, name='newpath'),
    path('mannagement/pathedit/<int:path_id>',
         views.pathediting, name='pedit'),
    path('account/', views.accdetails, name='accdetails'),
    path('account/changepassword', views.changepassword, name='passchange'),
    path('password_reset', auth_views.password_reset, name='password_reset'),
    path('password_reset/done', auth_views.password_reset_done,
         name='password_reset_done'),
    path('reset/<uidb64>/<token>',
         auth_views.password_reset_confirm, name='password_reset_confirm'),
    path('reset/done', auth_views.password_reset_complete,
         name='password_reset_complete'),
    path('newrequest/', views.req_type_pick, name='typepick'),
    path('newrequest/<passtype>', views.createrequest, name='newreq'),
    path('requests', views.req_list, name='req_list'),
    path('requests/<int:req_id>', views.req_details, name='req_details'),
    path('requests/queue', views.discussion_queue, name='queue'),
    path('requests/queue/<int:req_id>', views.req_discussion, name='req_dis'),
    path('requests/<int:req_id>/edit', views.req_edit, name='req_edit'),
    



]
